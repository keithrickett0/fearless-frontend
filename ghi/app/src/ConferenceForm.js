import React, { useEffect, useState } from "react";

function ConferenceForm() {
  const [locations, setLocations] = useState([])
  const [name, setName] = useState('')
  const [starts, setStart] = useState('')
  const [ends, setEnd] = useState('')
  const [description, setDescription] = useState('')
  const [presentation, setPresentation] = useState('')
  const [attendees, setAttendees] = useState('')
  const [location, setLocation] = useState('')

  const handleNameChange = (event) => {
    const value = event.target.value
    setName(value)
  }
  const handleStartDateChange = (event) => {
    const value = event.target.value
    setStart(value)
  }
  const handleEndDateChange = (event) => {
    const value = event.target.value
    setEnd(value)
  }
  const handleDescriptionChange = (event) => {
    const value = event.target.value
    setDescription(value)
  }
  const handlePresentationChange = (event) => {
    const value = event.target.value
    setPresentation(value)
  }
  const handleAttendeeChange = (event) => {
    const value = event.target.value
    setAttendees(value)
  }
  const handleLocationChange = (event) => {
    const value = event.target.value
    setLocation(value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault()

    const data = {}

    data.name = name
    data.starts = starts
    data.ends = ends
    data.description = description
    data.max_presentations = presentation
    data.max_attendees = attendees
    data.location = location
    console.log(data)

    const locationUrl = 'http://localhost:8000/api/conferences/'
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    }

    const response = await fetch(locationUrl, fetchConfig)
    if (response.ok) {
      const newConference = await response.json()
      console.log(newConference)

      setName('')
      setStart('')
      setEnd('')
      setDescription('')
      setPresentation('')
      setAttendees('')
      setLocation('')
    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/'
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      setLocations(data.locations)
      console.log(data)
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new conference</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={starts} onChange={handleStartDateChange} placeholder="Starts" required type="date" name="state_date" id="start_date" className="form-control" />
              <label htmlFor="start_date">Starts</label>
            </div>
            <div className="form-floating mb-3">
              <input value={ends} onChange={handleEndDateChange} placeholder="Ends" required type="date" name="end_date" id="end_date" className="form-control" />
              <label htmlFor="end_date">Ends</label>
            </div>
            <div className="form-floating mb-3">
              <textarea className="form-control" value={description} onChange={handleDescriptionChange} placeholder="description" id="floatingTextarea"></textarea>
              <label for="floatingTextarea">Decription</label>
            </div>
            <div className="form-floating mb-3">
              <input value={presentation} onChange={handlePresentationChange} placeholder="max_presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input value={attendees} onChange={handleAttendeeChange} placeholder="max_attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div>
            <div className="mb-3">
              <select value={location} onChange={handleLocationChange} required name="locations" id="locations" className="form-select">
                <option defaultValue="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ConferenceForm
